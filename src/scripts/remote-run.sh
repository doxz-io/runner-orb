#!/bin/bash

curl -o remote-runner.py https://bitbucket.org/doxz-io/scripts/raw/c4d1e564d9fbc28b302deb2ec3eda7c7e4043368/py/remote-runner.py && \
python3 remote-runner.py --source "$PARAM_SOURCE" --dest "$PARAM_DEST" --entry "$PARAM_ENTRY"